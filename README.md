# Code-Inspector

A lightweight Docker image and GitLab CI template for [codeinspectorio](https://github.com/codeinspectorio/citool). See their [documentation](http://doc.code-inspector.com/ci-support.html) and [website](https://www.code-inspector.com) for details.

## Using this project

Implementing `code-inspector` is easy. Just include this line in your `.gitlab-ci.yml`:

```yml
include:
  - remote: https://gitlab.com/jhctechnology/code-quality/code-inspector/raw/master/templates/.gitlab-ci-code-inspector.yml
```

The job will expect to run as part of a stage called `qa` (quality assurance), so if you don't have one, also include `qa` in your stages:

```yml
stages:
  - qa
```

### Branching

On your master branch, the CI job will run and check for any violations or duplications. On any branch other than master, the CI job will compare the current branch to the master branch and consider the differences in number of violations and duplicates, which we refer to as `INC_TECH_DEBT` or increased technical debt.

### Controlling job passing criteria

#### Master

By default, the CI job will exit with error if any violations are found, and exit successfully if there are no violations and any number of duplicates including zero. To change this behavior, override the variables below by including them in your `.gitlab-ci.yml` file after your `include` statement:

```yml
variables:
  CODE_INSPECTOR_ALLOW_VIOLATIONS: 'false'
  CODE_INSPECTOR_ALLOW_DUPLICATES: 'true
```

#### Other Branches

By default, the CI job will exit with error if the number of violations in the target branch is greater than the number in the master branch, and exit successfully if there are no increased violations and any number of increased duplicates or no change to the number of duplicates. To change this behavior, override the variables below by including them in your `.gitlab-ci.yml` file after your `include` statement:

```yml
variables:
  CODE_INSPECTOR_ALLOW_VIOLATIONS_INC_TECH_DEBT: 'false'
  CODE_INSPECTOR_ALLOW_DUPLICATES_INC_TECH_DEBT: 'true
```
